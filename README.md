# Ray Tracing version 1
## RTv1

School 21 (Ecole 42) students project, realise simple ray tracing 3D objects (secondary order surface) 
and simple light sources (point, directional and ambient)

This project developed, tested, builded and compiled on iMac 27" with MacOS and tiny student graphic library 
`MinilibX` (like sdl, but oversimplified). You can find any version of this lib for any OS on github / gitlab. 
For example, [here](https://github.com/42Paris/minilibx-linux) for linux.

You can use `CMakeList.txt` or `Makefile` for build and compile.

This repo has libgm and libft. 
The libgm is my own library where I create functions and structures to store and work with vectors `[1✕4]` 
and matrices `[4✕4]`. 
The libft is my own library too, but it was created as my first school project - an implementation 
of the C standard library.

After setup and compile you can write this in the bash:
```
> RTv1 FILE.rt 
```

For example, the repository contains a `config.rt`, its simple scene.

The `config.rt` contains 4 simple object, camera and light:
```
sphere
cylinder
cone
plane
light
camera
```
After the name of the object, light and camera, a colon is put and on a new line, starting with a tab, 
the characteristics of the object are written. Every object has an base attributes:
```
color
specular
data
center
reflection
data
```
And specific attributes:
```
sphere:
	center: 0.5, 1, 3, 0
	radius: 0.3
-
cylinder:
	axis: 0.0, -1, 0, 0
    radius: 1.0
-
cone:
	axis: 0.0, -1, 0, 0
    angle: 0.05
-
plane:
	norm: 0, 0, -1, 0
	data: 0.2, 0, 0, 0
```
Each config file should contain a description of the light and the camera. 
The light source and camera are described as here:
```
light:
	type: ambient
	intensive: 0.2
-
light:
	type: point
	intensive: 0.2
	point: -2, 5, 2, 0
-
light:
	type: dir
	intensive: 0.1
	point: 1, 4, 4, 0
-
camera:
	center: 0, 1, 0, 0
	screen: 160, 90
```

### Screenshots
![Screenshot_0](https://gitlab.com/MalianovIY/RTv1/-/raw/master/docs/0.jpg)
![Screenshot_1](https://gitlab.com/MalianovIY/RTv1/-/raw/master/docs/1.jpg)

### Subject
[Subject](https://cdn.intra.42.fr/pdf/pdf/1866/rtv1.en.pdf)

#### PS
Our team stores the [next project](https://github.com/rearming/RT) along 
this route (Graphic 3D-render) in the Team Lead account on Github.
